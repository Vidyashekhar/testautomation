package com.test.automation.basepage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

public  class downloadpage {

	
	public static WebDriver driver;
	
	@FindBy(linkText = "Downloads")
	WebElement downloads;
	
	@FindBy(id ="searchInput")
	WebElement searchBox;
	
	public downloadpage() {
		
		PageFactory.initElements(driver, this);
	}
	 
	@Test
	public void testpage() {
		
		System.setProperty("webdriver.chrome.driver","/Users/vidyadsathish/Downloads/chromedriver");
		driver = new ChromeDriver();
		driver.get("chrome://downloads/");
		
	}
	
}
